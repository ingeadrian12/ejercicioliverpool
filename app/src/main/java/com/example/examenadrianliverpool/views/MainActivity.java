package com.example.examenadrianliverpool.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.examenadrianliverpool.R;
import com.example.examenadrianliverpool.adapters.AdapterRecycleview;
import com.example.examenadrianliverpool.interactor.interactorMain.InteractorImplementsMain;
import com.example.examenadrianliverpool.model.Liverpool;
import com.example.examenadrianliverpool.model.Records;
import com.example.examenadrianliverpool.presenter.main.PresenterMainView;
import com.example.examenadrianliverpool.presenter.main.mainView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements mainView {
    private PresenterMainView presenterMainView;
    private InteractorImplementsMain interactorImplementsMain;
    public AdapterRecycleview adapterRecycleview;
    private RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView= findViewById(R.id.listas);


        init();



    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void error(String error) {

    }

    @Override
    public void success(List<Records> data) {

        if (data!=null){


              adapterRecycleview =new AdapterRecycleview(this,data);
              recyclerView.setAdapter(adapterRecycleview);

        }

    }

    private void init(){
        interactorImplementsMain = new InteractorImplementsMain();

        presenterMainView = new PresenterMainView(this,interactorImplementsMain,this);

        presenterMainView.callResponse("computadoras");

        recyclerView= findViewById(R.id.listas);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //  recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


    }
}
