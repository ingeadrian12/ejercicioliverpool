package com.example.examenadrianliverpool.presenter.main;

import com.example.examenadrianliverpool.model.Records;

import java.util.List;

public interface mainView {
    void  showProgress();
    void hideProgress();
    void  error(String error);
    void success(List<Records> data);
}
