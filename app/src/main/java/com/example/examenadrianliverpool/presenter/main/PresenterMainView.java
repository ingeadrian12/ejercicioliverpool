package com.example.examenadrianliverpool.presenter.main;

import android.content.Context;

import com.example.examenadrianliverpool.interactor.interactorMain.InteractorImplementsMain;
import com.example.examenadrianliverpool.interactor.interactorMain.interactorMain;
import com.example.examenadrianliverpool.model.Liverpool;
import com.example.examenadrianliverpool.model.Records;

import java.util.List;


public class PresenterMainView implements interactorMain {

    private mainView MainView;

    private InteractorImplementsMain interactorImplementsMain;

    private Context context;


    public PresenterMainView(mainView MainView, InteractorImplementsMain interactorImplementsMain, Context context) {
        this.interactorImplementsMain=interactorImplementsMain;
        this.MainView = MainView;
        this.context = context;
    }


    public void callResponse(String busqueda){
        if (MainView!=null && interactorImplementsMain!=null)
        {
            MainView.showProgress();
            interactorImplementsMain.RequestGet(this,this.context,busqueda);

        }
    }

    @Override
    public void onSuccess(List<Records> data) {

        if (MainView!=null){
            MainView.hideProgress();
            MainView.success(data);
        }

    }

    @Override
    public void Fail(String error) {

        if (MainView!=null){
            MainView.hideProgress();
            MainView.error(error);
        }

    }
}
