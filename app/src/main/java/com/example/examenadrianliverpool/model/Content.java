package com.example.examenadrianliverpool.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Content implements Serializable {


    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("mainContent")
    @Expose
    private List<MainContent> mainContent =null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<MainContent> getMainContent() {
        return mainContent;
    }

    public void setMainContent(List<MainContent> mainContent) {
        this.mainContent = mainContent;
    }
}
