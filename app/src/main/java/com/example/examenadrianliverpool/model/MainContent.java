package com.example.examenadrianliverpool.model;

import com.example.examenadrianliverpool.model.submodel.Contents2;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MainContent implements Serializable
{
    @SerializedName("contents")
    @Expose
    private List<Contents2> contents2=null;

    public List<Contents2> getContents2() {
        return contents2;
    }

    public void setContents2(List<Contents2> contents2) {
        this.contents2 = contents2;
    }
}
