package com.example.examenadrianliverpool.model.submodel;

import com.example.examenadrianliverpool.model.Records;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Contents2 implements Serializable
{

    @SerializedName("records")
    @Expose
    private List<Records> records =null;

    public List<Records> getRecords() {
        return records;
    }

    public void setRecords(List<Records> records) {
        this.records = records;
    }
}
