package com.example.examenadrianliverpool.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Records implements Serializable
{
    @SerializedName("productDisplayName")
    @Expose
    private List<String> productDisplayName=null;
    @SerializedName("salePrice")
    @Expose
    private List<String>salePrice=null;

    @SerializedName("sku.thumbnailImage")
    @Expose
    private List<String>thumbnailImage=null;

    public List<String> getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(List<String> productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public List<String> getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(List<String> salePrice) {
        this.salePrice = salePrice;
    }

    public List<String> getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(List<String> thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }
}
