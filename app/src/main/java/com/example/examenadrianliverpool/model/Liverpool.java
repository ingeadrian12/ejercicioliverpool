package com.example.examenadrianliverpool.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Liverpool implements Serializable
{
    @SerializedName("contents")
    @Expose
    private List<Content>Content =null;

    public List<com.example.examenadrianliverpool.model.Content> getContent() {
        return Content;
    }

    public void setContent(List<com.example.examenadrianliverpool.model.Content> content) {
        Content = content;
    }
}
