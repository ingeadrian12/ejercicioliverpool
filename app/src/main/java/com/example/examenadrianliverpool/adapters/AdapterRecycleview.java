package com.example.examenadrianliverpool.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.examenadrianliverpool.R;
import com.example.examenadrianliverpool.model.Liverpool;
import com.example.examenadrianliverpool.model.Records;

import java.util.List;

public class AdapterRecycleview extends RecyclerView.Adapter<AdapterRecycleview.ViewHolder>  {


    private Context context;

    private List<Records> data;
    public AdapterRecycleview(Context context, List<Records> data) {

        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.content, viewGroup, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

viewHolder.titulo.setText("Titulo: "+this.data.get(i).getProductDisplayName().get(0));
viewHolder.precio.setText("Precio: "+(this.data.get(i).getSalePrice().get(0)));



       Glide.with(context).load(this.data.get(i).getThumbnailImage().get(0))
               .into(viewHolder.imageView);
       // new LoadImage(viewHolder.imageView).execute(this.data.get(i).getThumbnailImage().get(0));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView titulo;
        private TextView precio;




    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView= itemView.findViewById(R.id.person_photo);
        titulo = itemView.findViewById(R.id.titulo);
        precio = itemView.findViewById(R.id.precio);



    }
}
}
