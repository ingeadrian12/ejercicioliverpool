package com.example.examenadrianliverpool.interactor.interactorMain;

import android.content.Context;

import com.example.examenadrianliverpool.model.Liverpool;
import com.example.examenadrianliverpool.model.MainContent;
import com.example.examenadrianliverpool.model.Records;
import com.example.examenadrianliverpool.model.submodel.Contents2;
import com.example.examenadrianliverpool.services.ConnectNetwork;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InteractorImplementsMain
{
    public void RequestGet(final interactorMain Interactor, Context context,String busqueda)
    {
        if (Interactor!=null && context!=null){

            final Call<Liverpool> response = ConnectNetwork.apiIcomSysService(context).ResponseDataJson(busqueda);

            response.enqueue(new Callback<Liverpool>() {
                @Override
                public void onResponse(Call<Liverpool> call, Response<Liverpool> response) {

                    if (response.isSuccessful()){

                       // List<Records> records = response.body().getContent().get(0).getMainContent().get(3).getContents2().get(1).getRecords();


                        records(response.body());

                        Interactor.onSuccess(records(response.body()));

                    }
                }

                @Override
                public void onFailure(Call<Liverpool> call, Throwable t) {

                   Interactor.Fail(t.getMessage());
                }
            });


        }


    }

    private List<Records> records(Liverpool liverpool){

        List<MainContent> mainContents = null;
        for (int i=0; i<liverpool.getContent().size();i++){

            for (int j=0; j<liverpool.getContent().get(i).getMainContent().size();j++){

                mainContents = liverpool.getContent().get(i).getMainContent();

            }
        }


        List<Contents2> contents2s =null ;
        for (int p=0;p<mainContents.size(); p++){
            contents2s = mainContents.get(p).getContents2();
        }


        List<Records> records=null;

        for (int h=0; h<contents2s.size(); h++){
            records = contents2s.get(h).getRecords();
        }

        return records;
}
}
